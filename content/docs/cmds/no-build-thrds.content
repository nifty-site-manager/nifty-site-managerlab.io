<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">cmd: <mono>no-build-thrds</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#eg">Example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for the <mono>no-build-thrds</mono> command is:
<div align="center">
<pre class="prettyprint inline lang-nift">
nift no-build-thrds (int)
nsm no-build-thrds (int)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>no-build-thrds</mono> command is for getting and setting the number of threads to use for things like building and checking the status of a project, it either takes no parameters and returns the configured number of threads to use, or takes a single integer parameter specifying the number of threads that should be used.
				</p>

				<p>
					<b>Note:</b> <mono>-n</mono> build threads represents using <mono>n</mono> times the number of cores on the machine threads.
				</p>

				<h4 id="eg">
					Example
				</h4>
				<p>
					Example of <mono>no-build-thrds</mono> being used:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
nift no-build-thrds
nsm no-build-thrds -1</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
