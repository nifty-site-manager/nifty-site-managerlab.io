<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">cmd: <mono>new-cont-ext</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#eg">Example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for the <mono>new-cont-ext</mono> command is:
<div align="center">
<pre class="prettyprint inline lang-nift">
nift new-cont-ext (name) ext
nsm new-cont-ext (name) ext
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>new-cont-ext</mono> command is for setting the content extension project wide or for a tracked file. If no name parameter is provided then it will update the content extension for the project, otherwise if a name is provided then the specified content extension will only be applied to that file.
				</p>

				<h4 id="eg">
					Example
				</h4>
				<p>
					Example of <mono>new-cont-ext</mono> being used:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
nift new-cont-ext ".md"
nsm new-cont-ext blog/post01 ".rst"</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
