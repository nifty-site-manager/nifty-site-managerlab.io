<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">cmd: <mono>track-from-file</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#eg">Example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for the <mono>track-from-file</mono> command is:
<div align="center">
<pre class="prettyprint inline lang-nift">
nift track-from-file file
nsm track-from-file file
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>track-from-file</mono> command is for tracking large volumes of new files to be generated during a build with <mono>Nift</mono>.
				</p>

				<p>
					The file outlining files to track should have one file specified on each line following the format:
<div align="center" style="margin-top:-30px; margin-bottom:10px">
<pre class="inline">
name (title) (content-extension) (template-path) (output-extension)
</pre>
</div>
					where:
					<ul>
						<li><mono>name</mono> is the name of the file you want to track;</li>
						<li><mono>title</mono> is the title, if not specified the <mono>name</mono> is used;</li>
						<li><mono>content-ext</mono> is the content extension to use, if not specified the project-wide content extension is used;</li>
						<li><mono>template-path</mono> is the template path to track with, if not specified the project-wide default template path is used;</li>
						<li><mono>output-ext</mono> is the output extension to track with, if not specified the project-wide output extension is used.</li>
					</ul>
				</p>

				<p>
					<b>Note:</b> the file with track information can have empty lines and comment lines beginning with '<mono>#</mono>' and you should quote any names, titles, extensions or paths containing spaces.
				</p>

				<p>
					<b>Note:</b> <mono>track-from-file</mono> and <a href="@pathtopage(docs/cmds/track-dir)">track-dir</a> are much faster for tracking large volumes of files than <a href="@pathtopage(docs/cmds/track)">track</a>.
				</p>

				<h4 id="eg">
					Example
				</h4>
				<p>
					Example of <mono>track-from-file</mono> being used:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
nift track-from-file to-track.txt
nsm track-from-file to-track.txt</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
