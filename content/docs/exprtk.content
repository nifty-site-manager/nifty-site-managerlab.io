<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">ExprTk</div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<center>
				<img src="@pathtofile(site/images/banner-planet-palms.png)" alt="Nift planet palms banner" width="500px" style="max-width:90%; border-radius:20px; margin-bottom: 60px">
			</center>

			<div class="content">
				<p>
					<mono>Nift</mono> has the C++ Mathematical Expression Toolkit Library (<mono>ExprTk</mono>) embedded (<a href="http://www.partow.net/programming/exprtk/index.html">official website</a>, <a href="https://github.com/ArashPartow/exprtk">GitHub</a>). <mono>ExprTk</mono> is a simple to use, easy to integrate and <a href="#benchmark">extremely efficient</a> run-time mathematical expression parser and evaluation engine. <mono>ExprTk</mono> supports numerous forms of functional, logical and vector processing semantics and is very easily extendible.
				</p>

				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<div>
					<ul>
						<li><a href="#interpreter">ExprTk interpreter</a></li>
						<li><a href="#scripts">Running ExprTk scripts</a></li>
						<li><a href="#exprtk-f++">ExprTk from f++</a></li>
						<li><a href="#exprtk-n++">ExprTk from n++</a></li>
						<li><a href="#exprtk-conditions">ExprTk with n++/f++ conditions</a></li>
						<li><a href="#exprtk-vars">ExprTk access to Nift variables</a></li>
						<li><a href="#nift-fns">Nift functions</a></li>
						<li><a href="#benchmark">Benchmark results</a></li>
					</ul>
				</div>

				<h4 id="interpreter">
					ExprTk interpreter
				</h4>
				<p>
					<mono>Nift</mono> has an <mono>ExprTk</mono> interpreter that you can start with either <mono>nsm interp -exprtk</mono> or <mono>nift interp -exprtk</mono>.
				</p>

				<p>
					In Nift's interpreter mode the prompt will just display the language. If you would like the prompt to also display the present working directory (up to using half the width of the console) you can switch to the shell mode using <mono>nsm_mode('sh')</mono>. You can switch back again with <mono>nsm_mode('interp')</mono>.
				</p>

				<p>
					You can switch to one of the other languages available in Nift's interpreter using <mono>nsm_lang('langStr')</mono> where <mono>langStr</mono> is one of <mnstr style="font-size:16px">f++</mnstr>, <mnstr style="font-size:16px">N++</mnstr>, <mono>lua</mono> or <mono>exprtk</mono>.
				</p>


				<h4 id="scripts">
					Running ExprTk scripts
				</h4>
				<p>
					If you have an <mono>ExprTk</mono> script saved in a file <mono>path/script-name.exprtk</mono> you can run it with either of the following:
				</p>
<div align="center">
<pre class="prettyprint inline">
nsm run path/script-name.exprtk
nift run path/script-name.exprtk
</pre>
</div>

				<p>
					If the script has a different extension, say <mono>.ext</mono>, you can run the script with either of the following:
				</p>
<div align="center">
<pre class="prettyprint inline">
nsm run -exprtk path/script-name.ext
nift run -exprtk path/script-name.ext
</pre>
</div>

				<h4 id="exprtk-f++">
					ExprTk from <mnstr style="font-size:16px">f++</mnstr>
				</h4>
				<p>
					You can run <mono>ExprTk</mono> code from <mnstr style="font-size:16px">f++</mnstr> using any of the following (without needing the comment syntax):
				</p>					

<div align="center">
<pre class="prettyprint inline">
exprtk(expression)
</pre>
</div>

<div align="center">
<pre class="prettyprint inline">
`expression`
</pre>
</div>

<div align="center">
<pre class="prettyprint inline">
exprtk
{
	// block of ExprTk code
}
</pre>
</div>

				<h4 id="exprtk-n++">
					ExprTk from n++
				</h4>
				<p>
					You can run <mono>ExprTk</mono> code from <mnstr style="font-size:16px">N++</mnstr> using any of the following (without needing the comment syntax):
				</p>					

<div align="center">
<pre class="prettyprint inline">
\@exprtk(expression)
</pre>
</div>

<div align="center">
<pre class="prettyprint inline">
\@`expression`
</pre>
</div>

<div align="center">
<pre class="prettyprint inline">
\@exprtk
{
	// block of ExprTk code
}</pre>
</div>

				<h4 id="exprtk-conditions">
					ExprTk with <mnstr style="font-size:16px">f++</mnstr>/<mnstr style="font-size:16px">N++</mnstr> conditions
				</h4>
				<p>
					You can use <mono>ExprTk</mono> with conditions for <mono>if</mono>, <mono>else-if</mono> and <mono>else</mono> statements, along with <mono>do-while</mono>, <mono>for</mono>, <mono>while</mono> loops. You can also use <mono>ExprTk</mono> for the post-loop increment code with <mono>for</mono> loops (ie. the code after the second <mono>;</mono>). For example:
				</p>
<div align="center">
<pre class="prettyprint inline">
for(int i=0; i<10; i+=1)
	console("i: ", i)</pre>
</div>

				<h4 id="exprtk-vars">
					ExprTk access to Nift variables
				</h4>
				<p>
					<mono>ExprTk</mono> has access to <mono>Nift</mono> variables of the following types: <mono>bool</mono>, <mono>int</mono>, <mono>double</mono>, <mono>char</mono>, <mono>string</mono> and <mono>std::vector@ent('<')double@ent('>')</mono>. You can simply use the variables as if they had been defined inside <mono>ExprTk</mono>, although a <mono>char</mono> is treated as a <mono>string</mono>, and a <mono>bool</mono> or an <mono>int</mono> is treated as a <mono>double</mono>.
				</p>

				<h4 id="nift-fns">
					Nift functions
				</h4>
				<p>
					The following functions specific to <mono>Nift</mono> are available inside <mono>ExprTk</mono> code.

					<center class="table">
						<table id="exprtk_nift_fns_table" class="alt">
							<thead>
								<tr>
									<th>syntax</th>
									<th>example</th>
									<th>about</th>
								</tr>
							</thead>
							<tr>
								<td><mono>cd(string)</mono></td>
								<td><mono>cd('~/')</td>
								<td>change directory</td>
							</tr>
							<tr>
								<td><mono>sys(string)</mono></td>
								<td><mono>sys('ls *.txt')</td>
								<td>execute system call/command</td>
							</tr>
							<tr>
								<td><mono>to_string(double)</mono></td>
								<td><mono>to_string(10)</td>
								<td>convert double to string</td>
							</tr>
							<tr>
								<td><mono>nsm_setnumber(string, number)</mono></td>
								<td><mono>nsm_setnumber('i', 0)</td>
								<td>set Nift variable from number</td>
							</tr>
							<tr>
								<td><mono>nsm_setstring(string, string)</mono></td>
								<td><mono>nsm_setstring('str', 'hello!')</td>
								<td>set Nift variable from string</td>
							</tr>
							<tr>
								<td><mono>nsm_tonumber(string)</mono></td>
								<td><mono>nsm_tonumber('i')</td>
								<td>get number from Nift variable</td>
							</tr>
							<tr>
								<td><mono>nsm_tostring(string)</mono></td>
								<td><mono>nsm_tostring('str')</td>
								<td>get string from Nift variable</td>
							</tr>
							<tr>
								<td><mono>nsm_write(ostream, params)</mono></td>
								<td><mono>nsm_write(console, 'x: ', x, endl)</td>
								<td>write to console, output file (ofile), or a stream</td>
							</tr>
							<tfoot>
								<tr>
									<th>syntax</th>
									<th>example</th>
									<th>about</th>
								</tr>
							</tfoot>
						</table>
					</center>
				</p>

				<h4 id="benchmark">
					Benchmark results
				</h4>
				<p>
					Results from <a href="https://github.com/ArashPartow/math-parser-benchmark-project">The Great C++ Mathematical Expression Parser Benchmark</a> are below:
				</p>
<div align="center">
<pre class="prettyprint inline">
Scores:
  #     Parser                  Type            Points   Score   Failures 
  -----------------------------------------------------------------------
  00    ExprTk                  double          901      100     0
  01    ExprTkFloat             float           740       82     9
  02    muparserSSE             float           726       93     9
  03    METL                    double          686       52     0
  04    FParser 4.5             double          584       43     0
  05    atmsp 1.0.4             double          530       38     2
  06    muparser 2.2.4          double          517       37     0
  07    muparser 2.2.4 (omp)    double          444       35     0
  08    MTParser                double          381       34     0
  09    MathExpr                double          360       29     2
  10    TinyExpr                double          354       31     2
  11    Lepton                  double          134        8     2
  12    muparserx               double           86        5     0
</pre>
</div>
			</div>
		</section>
	</div>
</section>
