<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">Windows Install</div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<h4>
					Syntax highlighting with Notepad++
				</h4>
				<p>
					It is fairly straight forward to set <mono>Notepad++</mono> as the default program to open/modify your <mono>*.content</mono> and <mono>*.template</mono> files. Further if you go to <mono>Settings -> Style Configurator...</mono> then select language <mono>HTML</mono> you can add <mono>content template</mono> to the <mono>User Ext:</mono> text box which will then have <mono>Notepad++</mono> automatically use html syntax highlighting for all <mono>*.content</mono> and <mono>*.template</mono> files (pretty nifty!).
				</p>

				<h4>
					Windows installation using <mono>Chocolatey</mono>
				</h4>
				<center><img src="@pathtofile{!p}(site/images/chocolatey.svg)" alt="Chocolatey logo" width=350px style="max-width:70%; margin-top: 20px; margin-bottom: 20px"></center>

				<p>
					You can install <mono>Nift</mono> on Windows using <mono>Chocolatey</mono>. First <a href="https://chocolatey.org/docs/installation">install</a> <mono>Chocolatey</mono>, then follow the instructions <a href="https://chocolatey.org/packages/nift">here</a>, or alternatively enter:
				</p>

				<div align="center">
<pre>
choco install nift
</pre>
				</div>

				<p>
					You can find compiler options including building with a normal version of <mono>Lua</mono> on the documentation page for <a href="@pathto(docs/make)">make</a>.
				</p>

				<p>
					To uninstall <mono>Nift</mono> enter <mono>choco uninstall nift</mono>.
				</p>

				<h4>
					Windows installation from executable
				</h4>

				<p>
					Download the latest <mono>Windows.zip</mono> from <a href="https://github.com/nifty-site-manager/nsm/releases">releases</a> then place the extracted versions of <mono>lua51.dll</mono>, <mono>nift.exe</mono> and <mono>nsm.exe</mono> anywhere in your user's path, eg. in <mono>C:\Windows\system32</mono>.
				</p>

				<h4>
					Installing <mono>Nift</mono> on Windows from source using Command Prompt
				</h4>
				<p>
					Download <a href="https://gitlab.com/nifty-site-manager/nsm/repository/master/archive.zip">nsm-master-*.zip</a>, then follow these steps to compile and install:
					<ol>
						<li>Install <a href="http://gnuwin32.sourceforge.net/packages/make.htm">GnuWin32</a> (Make for Windows) [or <mono>choco install gnuwin</mono>] and <a href="https://sourceforge.net/projects/mingw-w64/">mingw</a> [or <mono>choco install mingw</mono>];</li>
						<li>Open both a Command Prompt window and a Command Prompt (Admin) window (right click command prompt and select <mono>run as administrator</mono>);</li>
						<li>Enter <mono>path=%path%;C:\Program Files (x86)\GnuWin32\bin;</mono> in to both command prompts to add <mono>make</mono> to your path;</li>
						<li>In both command prompts change directory to where the nsm source code is;</li>
						<li>From the regular command prompt enter <mono>make</mono>;</li>
						<li>Move <mono>nsm.exe</mono> and <mono>nift.exe</mono> to somewhere in your user's path, eg. <mono>C:\Windows\system32</mono>;</li>
						<li>Delete both the file <mono>nsm-master-*.zip</mono> and the directory <mono>nsm-master-*</mono> containing the uncompiled/source code.</li>
					</ol>

					Note you may need to open a new command prompt window for <mono>nsm</mono> to start working.
				</p>

				<p>
					<b>Note:</b> See <a href="@pathtopage(docs/make)">here</a> for information about the variables you can pass to the Makefile when compiling <mono>Nift</mono>.
				</p>

				<h4>
					Uninstalling <mono>Nift</mono>
				</h4>
				<p>
					Should you ever want to remove <mono>nsm</mono> from your machine, delete <mono>nsm.exe</mono> and <mono>nift.exe</mono> from wherever you installed them.
				</p>

				<h4>
					Installing <mono>Nift</mono> on Windows from source using Git Bash
				</h4>
				<h4>
					Installing a <mono>c++</mono> compiler
				</h4>

				<p>
					If you do not already have a <mono>c++</mono> compiler then you will need to install one.
				</p>

				<p>
					On Windows I typically just use <a href="http://www.codeblocks.org/">code::blocks</a> which you can get to download/install with MinGW as the <mono>c++</mono> compiler.
				</p>

				<h4>
					Installing <mono>Git Bash</mono>, <mono>mingw</mono> and <mono>make</mono>
				</h4>

				Install <a href="https://git-scm.com/downloads">Git</a> [or <mono>choco install git</mono>, which should come with Git Bash as well, and install <a href="https://sourceforge.net/projects/mingw-w64/">mingw</a> [or <mono>choco install mingw</mono>]. 

				Get make from <a href="https://gist.github.com/evanwill/0207876c3243bbb6863e65ec5dc3f058">here</a> [or <mono>choco install make</mono>], ie:
				<ol>
					<li>
						Go to <a href="https://sourceforge.net/projects/ezwinports/files/">here</a>;
					</li>
					<li>
						download <mono>make-<i>version</i>-without-guile-w32-bin.zip</mono> (get the version without guile);
					</li>
					<li>
						Extract the contents of the zip file;
					</li>
					<li>
						Copy contents to <mono>Git/mingw64</mono>.
					</li>
				</ol>

				<h4>
					Installing <mono>Nift</mono>
				</h4>

				Download <a href="https://gitlab.com/nifty-site-manager/nsm/repository/master/archive.zip">nsm-master-*.zip</a>, then follow these steps to compile and install:

				<ol>
					<li>
						Extract <mono>nsm-master-*</mono> from <mono>nsm-master-*.zip</mono>;
					</li>
					<li>
						Open a <mono>Git Bash</mono> window (right click the desktop for example) and change directory to <mono>~/Downloads/nsm-master-*</mono>;
					</li>
					<li>
						Compile <mono>nsm</mono> by running <mono>make</mono>;
					</li>
					<li>
						Install <mono>nsm</mono> by running <mono>make git-bash-install</mono> (note - you may need to open a new terminal window for nsm installation to be recognised);
					</li>
					<li>
						Delete both the file <mono>nsm-master-*.zip</mono> and the directory <mono>nsm-master-*</mono> containing the uncompiled code. 
					</li>
				</ol>

				<p>
					For example, if you unzipped <mono>nsm-master-*</mono> inside your downloads folder, then steps 1-4 are achieved by entering the following into your terminal window:

					<div align="center">
<pre class="prettyprint inline rounded">
cd ~/Downloads/nsm-master-*
make
make git-bash-install
</pre>
					</div>
				</p>

				<p>
					You should now have a functioning version of <mono>nsm</mono> installed on your Windows machine. 
				</p>

				<h4>
					Uninstalling <mono>Nift</mono>
				</h4>
				<p>
					Should you ever want to remove <mono>nsm</mono> from your machine, either run <mono>make git-bash-uninstall</mono> from the source code directory or run <mono>sudo rm -f ~/bin/nsm ~/bin/nift</mono> from any Git Bash terminal window.
				</p>
			</div>
		</section>
	</div>
</section>
