<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>cd</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>cd</mono> calls is:
<div align="center" style="margin-top:-15px; margin-bottom:25px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-27px">
cd(path)
cd path 
</pre>
</div><br>
<div align="center" style="margin-top:-15px; margin-bottom:15px">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-27px">
\@cd(path)
\@cd path 
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					<mono>cd</mono> is the <u>change directory</u> function, it takes a single parameter that should be a path to change directory to.
				</p>

				<p>
					<b>Note:</b> <mono>Nift</mono> will skip to the first non-whitespace (ie. to the first character that is not a space, tab or newline) after a <mono>cd</mono> call and inject it to the output file where the call started. If you want to prevent <mono>Nift</mono> from doing this put a '<mono>!</mono>' after the call, eg.:
<div align="center">
<pre class="prettyprint inline lang-nift">
\@cd ~/;!
\@cd(~/)!
</pre>
</div>
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Examples of <mono>cd</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
cd("~/Downloads")
cd /usr/local/bin
cd "C:\Windows\system32"</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>cd</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@cd("~/Downloads")
\@cd /usr/local/bin
\@cd "C:\Windows\system32)"</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
