<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>if</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#options">Options</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>if</mono> statements is:
<div align="center" style="margin-top:-15px; margin-bottom:137px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-127px">
if{options}(condition)
{
	//code-block
}
else-if(condition)
{
	//code-block
}
else
{
	//code-block
}
</pre>
</div><br>
<div align="center" style="margin-top:-15px; margin-bottom:137px">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-127px">
\@if{options}(condition)
{
	//code-block
}
else-if(condition)
{
	//code-block
}
else
{
	//code-block
}
</pre>
</div>
				</p>

				<p>
					<b>Note:</b> Single-line code blocks do not need to be enclosed in parentheses.
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>if</mono> function takes a single parameter specifying a condition and the call is followed by a code-block, if the condition does not evaluate to <mono>0</mono> then the code-block is parsed and all following <mono>else-if</mono> and <mono>else</mono> statements are skipped. An <mono>if</mono> call can be followed by any number of <mono>else-if</mono> statements and can optionally be ended with a single <mono>else</mono> statement to be run if none, all of which works as you would expect from other languages. 
				</p>

				<p>
					<b>Note:</b> <mono>f++</mono> is used for <mono>if</mono> and <mono>else-if</mono> conditions, even for <mono>n++</mono>. If you accidentally use <mono>n++</mono> for any of the conditions it will most often run without any syntax or semantic errors anyway. 
				</p>

				<p>
					<b>Note:</b> If not writing to the output file <mono>Nift</mono> will skip to the first non-whitespace (ie. to the first character that is not a space, tab or newline) after an <mono>if</mono> statement and inject it to the output file where the call started. If you want to prevent <mono>Nift</mono> from doing this put a '<mono>!</mono>' after the statement, eg.:
<div align="center">
<pre class="prettyprint inline lang-nift">
\@if{!o}(condition)
{
	# block
}!
</pre>
</div>
				</p>

				<h4 id="options">
					Options
				</h4>
				<p>
					The following options are available for <mono>if</mono> statements:

					<center class="table">
						<table id="if_options_table" class="alt">
							<thead>
								<tr>
									<th>option</th>
									<th>description</th>
								</tr>
							</thead>
							<tr>
								<td><mono>f++</mono></td>
								<td>parse code-block with <mono>f++</mono></td>
							</tr>
							<tr>
								<td><mono>n++</mono></td>
								<td>parse code-block with <mono>n++</mono></td>
							</tr>
							<tr>
								<td><mono>!o</mono></td>
								<td>do not add output</mono></td>
							</tr>
							<tr>
								<td><mono>o</mono></td>
								<td>add output</mono></td>
							</tr>
							<tr>
								<td><mono>s</mono></td>
								<td>add scope</mono></td>
							</tr>
							<tr>
								<td><mono>!s</mono></td>
								<td>do not add scope</mono></td>
							</tr>
							<tfoot>
								<tr>
									<th>option</th>
									<th>description</th>
								</tr>
							</tfoot>
						</table>
					</center>
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>if</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
int i=0
if(i < 10)
	console("this will print")
else-if(i>10)
	console("this will not print")
else
	console("this also will not print")</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>if</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@int i=0
\@if(i < 10)
	\@console("this will print")
else-if(i>10)
	\@console("this will not print")
else
	\@console("this also will not print")</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
