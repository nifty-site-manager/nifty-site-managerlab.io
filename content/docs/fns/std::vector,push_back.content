<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title" style="overflow: hidden; padding-left: 10px; padding-right: 10px"><p style="overflow-x: auto;">fn:&nbsp;<mono>std::vector.push_back</mono></p></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>std::vector.push_back</mono> calls is:
<div align="center" style="margin-top:-15px; margin-bottom:40px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-30px">
name.push_back(items)
std::vector.push_back(name, items)
</pre>
</div><br>
<div align="center" style="margin-top:-15px; margin-bottom:40px">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-30px">
\@name.push_back(items)
\@std::vector.push_back(name, items)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>std::vector.push_back</mono> function is for pushing items on to the back of standard C++ vectors, it takes at least two parameters with the first parameter being the name of a standard C++ vector variable and the remainder of the parameters being items to push on the back. As a member function it takes at least one parameter, all of which should be items to push on the back.
				</p>

				<p>
					<b>Note:</b> For large scale projects you will find specifying the <mono>!mf</mono> option to not add member functions during definitions and using <mono>std::vector.push_back</mono> is faster than using the <mono>push_back</mono> member function.
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>std::vector.push_back</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
std::vector<double> v
std::vector.push_back(v, 2.0, 5.3, 3.26)
v.push_back(3.1415)
console(v.at(1))</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>std::vector.push_back</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@std::vector<double> v
\@std::vector.push_back(v, 2.0, 5.3, 3.26)
\@v.push_back(3.1415)
\@console(v.at(1))</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
