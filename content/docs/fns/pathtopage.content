<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>pathtopage</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>pathtopage</mono> calls is:
<div align="center" style="margin-top:-15px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-15px">
pathtopage(string)
</pre>
</div><br>
<div align="center">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-15px">
\@pathtopage(string)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>pathtopage</mono> function takes a single string parameter specifying a tracked name and returns the relative path from the file being built to the output file for the tracked name specified by the parameter. 
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>pathtopage</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums">
<a href="\@pathtopage(index)">home</a></pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
