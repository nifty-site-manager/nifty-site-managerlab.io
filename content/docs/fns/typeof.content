<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>typeof</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>typeof</mono> calls is:
<div align="center" style="margin-top:-15px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-15px">
typeof(var)
</pre>
</div><br>
<div align="center">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-15px">
\@typeof(var)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>typeof</mono> function takes a single variable parameter and returns the type of the variable.
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>typeof</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
:=(string, str="hello, world!")
console(typeof(str))</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>typeof</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@:=(string, str="hello, world!")
\@console(\@typeof(str))</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
