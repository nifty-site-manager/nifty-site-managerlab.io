<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>system</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#options">Options</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>sys</mono>/<mono>system</mono> calls is:
<div align="center" style="margin-top:-15px; margin-bottom:25px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-27px">
sys{options}(string)
system{options}(string)
</pre>
</div><br>
<div align="center" style="margin-top:-15px; margin-bottom:15px">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline lang-nift" style="margin-bottom:-27px">
\@sys{options}(string)
\@system{options}(string)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>sys</mono>/<mono>system</mono> function is for executing system calls/commands, it either:
					<ul>
						<li>takes a single string parameter which is the system call/command to execute; or</li>
						<li>takes zero parameters and the call is followed by a code-block to execute</li>
					</ul>
				</p>

				<p>
					<b>Note:</b> <mono>system</mono> calls are run from the project root directory so all paths should start from there. Also do not change directory in your system calls or scripts/programs they call as it will mess up the other threads that are also building output files. If you really must change directories in your scripts then you will have to set the number of build threads to <mono>1</mono> and change back to the project root directory before each script ends.
				</p>

				<p>
					<b>Note:</b> <mono>system</mono> calls do not scale very well to hundreds of thousands of calls, it is much better to combine as much as possible in to as few <mono>script</mono>/<mono>system</mono> calls as possible for very large projects (moving stuff to the pre/post build scripts is the best place, you can output stuff to file and process the files when building). For example if possible it is much faster to have a few pre-build scripts to download text from multiple urls using cURL, and/or make all the api calls, all the database queries, and work on JSON data needed and distribute the needed output in to different files to be inputted when needed. Note that file-specific build scripts have the benefit of running in parallel to each other, whereas project-wide build scripts do not.
				</p>

				<h4 id="options">
					Options
				</h4>
				<p>
					The following options are available for <mono>sys</mono>/<mono>system</mono> calls:

					<center class="table">
						<table id="sys/system_options_table" class="alt">
							<thead>
								<tr>
									<th>option</th>
									<th>description</th>
								</tr>
							</thead>
							<tr>
								<td><mono>content</mono></td>
								<td>add content file path of file being built as first input parameter to script/program</td>
							</tr>
							<tr>
								<td><mono>console</mono></td>
								<td>print script output to console</td>
							</tr>
							<tr>
								<td><mono>inject</mono> or <mono>inj</mono></td>
								<td>inject in to file being built after parsing with underlying shell</td>
							</tr>
							<tr>
								<td><mono>!o</mono></td>
								<td>do not add script output to file being built</td>
							</tr>
							<tr>
								<td><mono>pb</mono></td>
								<td>parse code-block following call before saving to script path for running</td>
							</tr>
							<tr>
								<td><mono>raw</mono></td>
								<td>inject in to file being built raw</td>
							</tr>
							<tr>
								<td><mono>!ret</mono></td>
								<td>do not return script/program return value</td>
							</tr>
							<tfoot>
								<tr>
									<th>option</th>
									<th>description</th>
								</tr>
							</tfoot>
						</table>
					</center>
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Examples of <mono>system</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
system("ls *.txt")
system("./script.rb")</pre>
</div>
				</p>

				<p>
<div align="center">
<pre class="prettyprint inline linenums lang-nift">
system
{
	x=10
	echo $x
}</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Examples of <mono>system</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@system("ls *.txt")
\@system('pandoc -o out.html file.md')
\@system("curl -sS https://pastebin.com/raw/atjKuxY6")
\@system("./script.rb")</pre>
</div>
				</p>

				<p>
<div align="center">
<pre class="prettyprint inline linenums lang-nift">
\@system
{
	x=10
	echo $x
}</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
