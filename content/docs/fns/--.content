<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>--</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>--</mono> calls is:
<div align="center" style="margin-top:-15px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-30px">
--param
--(params)
</pre>
</div><br>
<div align="center" style="margin-top:15px">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-30px">
\@--param
\@--(params)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					<mono>--</mono> is the <u>pre-decrement</u> operator, it takes a non-zero number of parameters that should all be number variables, decrements each of them and returns the value of the first parameter before being decremented.
				</p>

				<p>
					<b>Note:</b> It is typically faster to use <a href="@pathtopage(docs/exprtk)">exprtk</a> for decrementing variables, especially with the post-loop code with <mono>for</mono> loops.
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>--</mono> being used with <mono>f++</mono>:					

<div align="center" style="margin-top:-10px">
<pre class="prettyprint inline linenums">
int a=0, b=0
console(--a)
console(--(a, b))</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>--</mono> being used with <mono>n++</mono>:					

<div align="center" style="margin-top:-10px">
<pre class="prettyprint inline linenums">
\@int a=0, b=0
\@console(\@--a)
\@console(\@--(a, b))</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
