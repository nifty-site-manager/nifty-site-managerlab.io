<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>is_const</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>is_const</mono> calls is:
<div align="center" style="margin-top:-15px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-15px">
is_const(variable)
</pre>
</div><br>
<div align="center">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-15px">
\@is_const(variable)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					The <mono>is_const</mono> function takes a single variable or function as a parameter and returns whether the variable or function is a constant.
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>is_const</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums">
:={const}(int, a)
:=(int, b)
console(is_const(a))
console(is_const(b))</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>is_const</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums">
\@:={const}(int, a)
\@:=(int, b)
\@console(\@is_const(a))
\@console(\@is_const(b))</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
