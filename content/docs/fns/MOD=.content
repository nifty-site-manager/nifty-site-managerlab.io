<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">fn: <mono>%=</mono></div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<div class="content">
				<a href="#contents" class="to-top">[contents]</a>

				<h4 id="contents">
					Contents
				</h4>
				<ul>
					<li><a href="#syntax">Syntax</a></li>
					<li><a href="#description">Description</a></li>
					<li><a href="#f++-eg">f++ example</a></li>
					<li><a href="#n++-eg">n++ example</a></li>
				</ul>

				<h4 id="syntax">
					Syntax
				</h4>
				<p>
					The syntax for <mono>%=</mono> calls is:
<div align="center" style="margin-top:-15px">
<mono>f++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-15px">
%=(params)
</pre>
</div><br>
<div align="center">
<mono>n++</mono>: &nbsp; <pre class="prettyprint inline" style="margin-bottom:-15px">
\@%=(params)
</pre>
</div>
				</p>

				<h4 id="description">
					Description
				</h4>
				<p>
					<mono>%=</mono> is the <u>modulo assignment</u> function, it takes two parameters $p_1$, $p_2$, the first should be an integer variable and the second should be an integer, the function sets $p_1 = p_1 \mod p_2$.
				</p>

				<p>
					<b>Note:</b> It is typically faster to use <a href="@pathtopage(docs/exprtk)">exprtk</a> for the modulo function, plus the syntax is nicer.
				</p>

				<h4 id="f++-eg">
					f++ example
				</h4>
				<p>
					Example of <mono>%=</mono> being used with <mono>f++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums">
:=(int, a=5, b=3)
%=(a, b)
console(a)</pre>
</div>
				</p>

				<h4 id="n++-eg">
					n++ example
				</h4>
				<p>
					Example of <mono>%=</mono> being used with <mono>n++</mono>:					

<div align="center">
<pre class="prettyprint inline linenums">
\@:=(int, a=5, b=3)
\@%=(a, b)
\@console(a)</pre>
</div>
				</p>
			</div>
		</section>
	</div>
</section>
